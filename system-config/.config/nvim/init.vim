" init.vim

set nocompatible
filetype off

call plug#begin()
" All plugins must be placed between the lines
" ===========
Plug 'scrooloose/nerdtree'
" ===========
call plug#end()

filetype on
filetype plugin on
filetype plugin indent on

" General Settings
set encoding=utf-8
syntax enable
set number
set ruler

set tabstop=4                               " 4 whitespaces for tabs visual presentation
set shiftwidth=4                            " shift lines by 4 spaces
set smarttab                                " set tabs for a shifttabs logic
set expandtab                               " expand tabs into spaces
set autoindent                              " indent when moving to the next line while writing code
set nowrap                                  " Prevent text from wrapping around window

set cursorline                              " shows line under the cursor's line
set showmatch                               " shows matching part of bracket pairs (), [], {}o

set backspace=indent,eol,start              " backspace removes all (indents, EOLs, start) What is start?
set secure                                  " prohibit .vimrc files to execute shell, create files, etc...

set mouse=n                                 " Enabling mouse support

" Terminal
tnoremap <Esc> <C-\><C-n>

" NERDTree Settings
let g:NERDTreeWinPos = "right"
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1

map <C-n> :NERDTreeToggle<CR>

autocmd vimenter * NERDTree | wincmd p
autocmd BufEnter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
