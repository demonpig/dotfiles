# dotfiles/bash/.bash_profile
#
# Description:
# Profile config specific to bash.

# Environment Variables
PS1="[\u@\h \w]\n$ "
PATH="$PATH:$HOME/bin" ; export PATH

# Aliases
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Aliases specific to operating system
USER_OS="$(uname)"
if [[ $USER_OS == "Darwin" ]]; then
ANDROID_SDK_ROOT="/usr/local/share/android-sdk" ; export ANDROID_SDK_ROOT
alias ll='ls -lG'
alias ls='ls -G'
elif [[ $USER_OS == "Linux" ]]; then
VAGRANT_DEFAULT_PROVIDER="libvirt" ; export VAGRANT_DEFAULT_PROVIDER
GTK_IM_MODULE="fcitx" ; export GTK_IM_MODULE
QT_IM_MODULE="fcitx" ; export QT_IM_MODULE
XMODIFIERS="@im=fcitx" ; export XMODIFIERS
alias ll='ls -l --color'
alias ls='ls --color'
fi

alias stow='stow -t $HOME'
alias nvim='nvim -u ~/.config/nvim/init.vim'
