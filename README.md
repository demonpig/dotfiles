# dotfiles
## Installation and Configuration Steps
### Initial steps
#### MacOS
Install homebrew

```bash
xcode-select --install
```

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Install git and stow

```bash
brew install git stow
```

#### openSUSE
Install packages

```bash
zypper install git stow neovim
```

#### Final
Clone this repo

```bash
git clone https://gitlab.com/demonpig/dotfiles.git ~/.dotfiles
```

Use stow to setup dotfiles

```bash
pushd ~/.dotfiles ; stow bash ; stow system-config ; popd
```

### Applications
#### Neovim

Download the vim.plug

```
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

After setting up the dotfiles, you will want to execute the following in nvim.
Ignore any warning messages that might appear when first starting Neovim. This 
is caused by the init.vim having commands that are not yet available. 

```
nvim
:PlugInstall
:UpdateRemotePlugins
:q!
:q!
```
